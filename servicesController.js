'use strict';


var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/";
var ObjectID = require('mongodb').ObjectID; 

exports.list_all_complaints = function(req, res) {
    MongoClient.connect(url, function(err, db) {
        if (err) {
            throw err;
        }
        var dbo = db.db("customerComplaints");
        dbo.collection("complaintsList").find().toArray(function(err, result) {
            if (err) {
                res.send(err);
            } else {
                res.json(result);
            }
            db.close();
        });
    });
};

exports.read_a_complaint = function(req, res) {
    MongoClient.connect(url, function(err, db) {
        if (err) {
            throw err;
        }
        var dbo = db.db("customerComplaints");
        dbo.collection("complaintsList").find({'createdBy': req.query.id}).toArray(function(err, result) {
            //console.log(result, req.query.id);
            if (err) {
                res.send(err);
            } else {
                res.json(result);
            }
            db.close();
        });
    });
};

exports.create_a_complaint = function(req, res) {
    MongoClient.connect(url, function(err, db) {
        if (err) {
            throw err;
        }
        var dbo = db.db("customerComplaints");
        dbo.collection("complaintsList").insert(req.body, function(err, result) {
            //console.log(result, req.query.id);
            if (err) {
                res.send(err);
            } else {
                res.json(result);
            }
            db.close();
        });
    });
};

exports.update_a_complaint = function(req, res) {
    MongoClient.connect(url, function(err, db) {
        if (err) {
            throw err;
        }
        var dbo = db.db("customerComplaints"); console.log(req.body)
        dbo.collection("complaintsList").update({'_id': ObjectID(req.body._id)}, { $set : { 'status' : req.body.status}}, function(err, result) {
            console.log(result, req.body.id);
            if (err) {
                res.send(err);
            } else {
                res.json(result);
            }
            db.close();
        });
    });
};

exports.create_user = function(req, res) {
    MongoClient.connect(url, function(err, db) {
        if (err) {
            throw err;
        }
        var dbo = db.db("customerComplaints");
        dbo.collection("users").find({'username': req.body.username}).toArray(function(err, result) {
            //console.log(result);
            if (err) {
                res.send(err);
            } else {
                if (result.length == 0 ) {
                    dbo.collection("users").insert(req.body, function(err, result) {
                        //console.log(result, req.query.id);
                        if (err) {
                            res.send(err);
                        } else {
                            res.json(result);
                        }
                        db.close();
                    });
                }else{
                    res.json({'status': 0, message: 'User already Registered..'});
                }
            }
            db.close();
        });

    });
};

exports.read_a_user = function(req, res) {
    MongoClient.connect(url, function(err, db) {
        if (err) {
            throw err;
        }
        var dbo = db.db("customerComplaints");
        dbo.collection("users").findOne({'username': req.body.username}, function(err, result) {
            //console.log(result, req.body);
            if (err) {
                res.send(err);
            } else {
                if (!result) {
                    res.json({'status': 0, message: 'User not found.. Please sign up and try again..'});
                } else if (result.password == req.body.password) {
                    res.json({
                        'status': 1,
                         'user': {
                            'userName' : result.username,
                            'firstName' : result.firstName,
                            'lastName' : result.lastName,
                            'role' : result.role
                        }
                    });
                } else {
                    res.json({'status': 0, message: 'Password Mismatch..'});
                }
            }
            db.close();
        });
    });
};